/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.nsu.trofimova.calculator;

import java.io.IOException;
import java.io.Reader;

/**
 *
 * @author ekaterina
 */
public class Lexer {

    private Reader reader;
    private Lexeme savedLexeme;

    public Lexer(Reader r) {
        reader = r;
    }

    public Lexeme getLexeme() throws IOException, LexerException {

        if (savedLexeme != null) {
            Lexeme tempLexeme = savedLexeme;
            savedLexeme = null;
            return tempLexeme;
        }

        Lexeme numberLexeme = new Lexeme();
        int character;
        boolean isdigit = true;
        
        //skip
        do {
            character = reader.read();
        } while (character == ' ' || character == '\t' || character == '\n');

        //check for digit
        do {

            if (!Character.isDigit(character)) {
                isdigit = false;
                break;
            }

            numberLexeme.setType(LexemeType.DIGIT);

            switch (character) {
                case '1':
                    numberLexeme.setVal(numberLexeme.getVal() * 10 + 1);
                    break;
                case '2':
                    numberLexeme.setVal(numberLexeme.getVal() * 10 + 2);
                    break;
                case '3':
                    numberLexeme.setVal(numberLexeme.getVal() * 10 + 3);
                    break;
                case '4':
                    numberLexeme.setVal(numberLexeme.getVal() * 10 + 4);
                    break;
                case '5':
                    numberLexeme.setVal(numberLexeme.getVal() * 10 + 5);
                    break;
                case '6':
                    numberLexeme.setVal(numberLexeme.getVal() * 10 + 6);
                    break;
                case '7':
                    numberLexeme.setVal(numberLexeme.getVal() * 10 + 7);
                    break;
                case '8':
                    numberLexeme.setVal(numberLexeme.getVal() * 10 + 8);
                    break;
                case '9':
                    numberLexeme.setVal(numberLexeme.getVal() * 10 + 9);
                    break;
                case '0':
                    numberLexeme.setVal(numberLexeme.getVal() * 10);
                    break;
                default:
                    isdigit = false;
                    break;
            }

            character = reader.read();

        } while (isdigit);

        
        //skip
        while (character == ' ' || character == '\t' || character == '\n') {
            character = reader.read();
        }

        Lexeme symbolLexeme = new Lexeme();

        
        //check for symbol
        switch (character) {
            case '(':
                symbolLexeme.setType(LexemeType.OPENBRACKET);
                break;
            case ')':
                symbolLexeme.setType(LexemeType.CLOSEBRACKET);
                break;
            case '+':
                symbolLexeme.setType(LexemeType.PLUS);
                break;
            case '-':
                symbolLexeme.setType(LexemeType.MINUS);
                break;
            case '*':
                symbolLexeme.setType(LexemeType.MULT);
                break;
            case '/':
                symbolLexeme.setType(LexemeType.DIV);
                break;
            case '^':
                symbolLexeme.setType(LexemeType.POWER);
                break;
            case -1:
                symbolLexeme.setType(LexemeType.EOF);
                break;
            default:
                throw new LexerException();
        }

        if (numberLexeme.getType() == LexemeType.NONE) { //Обнаруженная лексема не числовая
            return symbolLexeme;
        }

        savedLexeme = symbolLexeme;
        return numberLexeme;

    }

    class LexerException extends Exception {

        public LexerException() {
        }
    }

}
