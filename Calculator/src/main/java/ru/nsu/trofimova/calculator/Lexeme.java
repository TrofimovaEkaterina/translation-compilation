/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.nsu.trofimova.calculator;

/**
 *
 * @author ekaterina
 */
public class Lexeme {

    private LexemeType type = LexemeType.NONE;
    private int val = 0;

    public void setType(LexemeType t) {
        type = t;
    }

    public LexemeType getType() {
        return type;
    }

    public void setVal(int v) {
        val = v;
    }

    public int getVal() {
        return val;
    }

}

enum LexemeType {
    PLUS, MINUS, MULT, DIV, DIGIT, OPENBRACKET, CLOSEBRACKET, NONE, EOF, POWER
};
