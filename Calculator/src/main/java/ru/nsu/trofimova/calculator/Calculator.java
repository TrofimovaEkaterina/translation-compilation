/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.nsu.trofimova.calculator;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import jdk.nashorn.internal.runtime.ParserException;
import ru.nsu.trofimova.calculator.Lexer.LexerException;

/**
 *
 * @author ekaterina
 */

//check

public class Calculator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Lexer lexer;

        try {
            lexer = new Lexer(new FileReader(args[1]));
        } catch (FileNotFoundException e) {
            System.err.println("File not found");
            return;
        }

        Parser parser = new Parser(lexer);

        try {
            System.out.print("Результат вычислений: " + parser.calculate());
        } catch (LexerException e) {
            System.err.println("Lexer failed: wrong format");
            return;
        } catch (Parser.ParseException e) {
            System.err.println("Parser failed: " + e.getMessage());
            return;
        } catch (IOException e) {
            System.err.println("IO operation failed: " + e.getMessage());
            return;
        }
    }

}
