/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.nsu.trofimova.calculator;

import java.io.IOException;
import static java.lang.Math.pow;
import ru.nsu.trofimova.calculator.Lexer.LexerException;

/**
 *
 * @author ekaterina
 */
public class Parser {

    Lexer lexer;
    Lexeme lexeme;

    public Parser(Lexer l) {
        lexer = l;
    }

    public int calculate() throws ParseException, IOException, LexerException {

        lexeme = lexer.getLexeme();
        int total = parseExpression();

        if (lexeme.getType() != LexemeType.EOF) {
            throw new ParseException("calculate exception: no EOF");
        }

        return total;

    }

    private int parseExpression() throws ParseException, IOException, LexerException {
        int total = parseTerm();

        do {

            switch (lexeme.getType()) {
                case PLUS:
                    lexeme = lexer.getLexeme();
                    total += parseTerm();
                    break;
                case MINUS:
                    lexeme = lexer.getLexeme();
                    total -= parseTerm();
                    break;
                case EOF:
                    return total;
                case CLOSEBRACKET:
                    return total;
                default:
                    throw new ParseException("parseExpression exception: no EOF or close bracket");
            }

        } while (true);

    }

    private int parseTerm() throws ParseException, IOException, LexerException {
        int total = parseFactor();

        do {

            switch (lexeme.getType()) {
                case DIV:
                    lexeme = lexer.getLexeme();
                    total = (int) (total / parseFactor());
                    break;
                case MULT:
                    lexeme = lexer.getLexeme();
                    total *= parseFactor();
                    break;
                default:
                    return total;
            }

        } while (true);

    }

    private int parseFactor() throws ParseException, IOException, LexerException {
        int total = parsePower();

        lexeme = lexer.getLexeme();         //Смотрим, есть ли показатель степени (Нет - просто сохраняем лексему на будущее)

        if (lexeme.getType() == LexemeType.POWER) {
            total = (int) pow(total, parseFactor());
            lexeme = lexer.getLexeme();     //Заранее сохраняем следующую лексему
        }

        return total;
    }

    private int parsePower() throws ParseException, IOException, LexerException {

        int total = 1;

        if (lexeme.getType() == LexemeType.MINUS) {
            lexeme = lexer.getLexeme();
            total = -1;
        }

        total *= parseAtom();

        return total;
    }

    private int parseAtom() throws ParseException, IOException, LexerException {

        if (lexeme.getType() == LexemeType.OPENBRACKET) {

            lexeme = lexer.getLexeme();          //Берем лексему после скобки
            int total = parseExpression();

            if (lexeme.getType() != LexemeType.CLOSEBRACKET) {      //Проверяем наличие закрывающей скобки
                throw new ParseException("parseAtom() exception: no close bracket");
            }

            return total;
        }

        if (lexeme.getType() == LexemeType.DIGIT) {
            return lexeme.getVal();
        }

        throw new ParseException("parseAtom() exception");
    }

    public class ParseException extends Exception {

        public ParseException() {
        }

        public ParseException(String message) {
            super(message);
        }
    }

}
