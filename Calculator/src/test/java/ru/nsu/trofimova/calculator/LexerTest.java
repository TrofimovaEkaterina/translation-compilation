/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.nsu.trofimova.calculator;

import java.io.IOException;
import java.io.StringReader;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import ru.nsu.trofimova.calculator.Lexer.LexerException;

/**
 *
 * @author ekaterina
 */
public class LexerTest {

    public LexerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getLexeme method, of class Lexer.
     */
    @Test
    public void testGetLexeme() throws Exception {
        System.out.println("getLexeme");
        String expression = "20*(10-4)";

        Lexer lexer = new Lexer(new StringReader(expression));

        Lexeme expResult = new Lexeme();

        expResult.setType(LexemeType.DIGIT);
        expResult.setVal(20);

        Lexeme result = lexer.getLexeme();

        assertEquals(expResult.getType(), result.getType());
        assertEquals(expResult.getVal(), result.getVal());

        expResult.setType(LexemeType.MULT);

        result = lexer.getLexeme();

        assertEquals(expResult.getType(), result.getType());

        expResult.setType(LexemeType.OPENBRACKET);

        result = lexer.getLexeme();

        assertEquals(expResult.getType(), result.getType());

        expResult.setType(LexemeType.MINUS);

        lexer.getLexeme();
        result = lexer.getLexeme();

        assertEquals(expResult.getType(), result.getType());

        expResult.setType(LexemeType.EOF);

        lexer.getLexeme();
        lexer.getLexeme();
        result = lexer.getLexeme();

        assertEquals(expResult.getType(), result.getType());

    }

    /**
     * Test of getLexeme method, of class Lexer.
     */
    @Test
    public void testGetLexemeException() throws Exception {
        System.out.println("getLexeme");
        String expression = "&20-3";

        Lexer lexer = new Lexer(new StringReader(expression));

        Lexeme expResult = new Lexeme();

        expResult.setType(LexemeType.DIGIT);
        expResult.setVal(20);

        boolean caught = false;
        
        try {
            Lexeme result = lexer.getLexeme();
        } catch (LexerException e) {
            caught = true;
        }
        
        assertTrue(caught);

    }
    
    
    /**
     * Test of getLexeme method, of class Lexer.
     */
    @Test
    public void testGetLexemeSpaceSkip() throws Exception {
        System.out.println("getLexeme");
        String expression = "20  - 3 + 2 0";

        Lexer lexer = new Lexer(new StringReader(expression));

        Lexeme expResult = new Lexeme();

        expResult.setType(LexemeType.DIGIT);
        expResult.setVal(3);

        lexer.getLexeme();
        lexer.getLexeme();
        Lexeme result = lexer.getLexeme();
        
        assertEquals(expResult.getVal(), result.getVal());
        
        lexer.getLexeme();
        
        boolean caught = false;
        
        try{
            lexer.getLexeme();
        }catch(Lexer.LexerException e){
            caught = true;
        }
        
        assertTrue(caught);

    }

}
