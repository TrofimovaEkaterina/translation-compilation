/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.nsu.trofimova.calculator;

import java.io.StringReader;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ekaterina
 */
public class ParserTest {

    public ParserTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of calculate method, of class Parser.
     */
    @Test
    public void testCalculate() throws Exception {
        System.out.println("calculate");

        String expression = "20*(10-4)/3";

        Parser parser = new Parser(new Lexer(new StringReader(expression)));

        int expResult = 40;
        int result = parser.calculate();

        assertEquals(expResult, result);

        expression = "20*(10-4)/3--1";

        parser = new Parser(new Lexer(new StringReader(expression)));

        expResult = 41;
        result = parser.calculate();

        assertEquals(expResult, result);

    }

    @Test
    public void testCalculateBracketException() throws Exception {
        System.out.println("calculate");

        String expression = "20*(10-4)/3*(4-2";

        Parser parser = new Parser(new Lexer(new StringReader(expression)));

        boolean caught = false;
        
        try {
            int result = parser.calculate();
        } catch (Parser.ParseException e) {
            caught = true;
        }
        
        assertTrue(caught);

    }
    
    
    @Test
    public void testCalculateLogicException() throws Exception {
        System.out.println("calculate");

        String expression = "20*(10-4)/3*---4";

        Parser parser = new Parser(new Lexer(new StringReader(expression)));

        boolean caught = false;
        
        try {
            int result = parser.calculate();
        } catch (Parser.ParseException e) {
            caught = true;
        }
        
        assertTrue(caught);

    }

}
